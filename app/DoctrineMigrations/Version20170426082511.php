<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170426082511 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql('ALTER TABLE tblProductData ADD floatprice DECIMAL NOT NULL, ADD intstock INT NOT NULL, CHANGE stmTimestamp stmTimestamp DATETIME NOT NULL ');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('ALTER TABLE tblProductData DROP floatprice, DROP intstock');
    }
}
