<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170426125646 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tblProductData CHANGE intProductDataId intProductDataId INT AUTO_INCREMENT NOT NULL, CHANGE dtmAdded dtmAdded DATETIME DEFAULT NULL, CHANGE dtmDiscontinued dtmDiscontinued DATETIME DEFAULT NULL, CHANGE stmTimestamp stmTimestamp DATETIME NOT NULL, CONVERT TO CHARACTER SET utf8');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tblProductData CHANGE dtmAdded dtmAdded VARCHAR(25) DEFAULT NULL COLLATE latin1_swedish_ci, CHANGE dtmDiscontinued dtmDiscontinued VARCHAR(25) DEFAULT NULL COLLATE latin1_swedish_ci, CHANGE stmTimestamp stmTimestamp VARCHAR(25) NOT NULL COLLATE latin1_swedish_ci, CHANGE intProductDataId intProductDataId INT UNSIGNED AUTO_INCREMENT NOT NULL');
    }
}
