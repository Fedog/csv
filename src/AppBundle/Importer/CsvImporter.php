<?php

namespace AppBundle\Importer;

use AppBundle\Steps\DateConversionStep;
use AppBundle\Steps\MapConversionStep;
use AppBundle\Steps\ValidationStep;
use AppBundle\Writer\CustomDoctrineWriter;
use Doctrine\ORM\EntityManager;
use Port\Csv\CsvReader;
use Port\Doctrine\DoctrineWriter;
use Port\Reader\ArrayReader;
use Port\Steps\StepAggregator;
use Port\SymfonyConsole\TableWriter;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Output\OutputInterface;

class CsvImporter
{
    private $em;
    public $imported;
    public $errors = [];
    public $stats;
    private $mapping;
    private $validator;

    public function __construct(EntityManager $entityManager, array $mapping, $validator)
    {
        $this->em = $entityManager;
        $this->mapping = $mapping;
        $this->validator = $validator;
    }

    public function importCSV($filePath, OutputInterface $output, $isTest)
    {
        $file = new \SplFileObject($filePath);
        $reader = $this->getCsvReader($file);

        $writer = new CustomDoctrineWriter($this->em, 'AppBundle:Product', $this->validator, $isTest);

        $validation = new ValidationStep();
        $dateConversionStep = new DateConversionStep();
        $mapConversionStep = new MapConversionStep($this->mapping);

        $workflow = $this->getWorkflow($reader, $writer, [$mapConversionStep, $validation, $dateConversionStep]);
        $workflow->process();

        $this->errors = array_merge($validation->failedProducts, $reader->getErrors(), $writer->failedItems);

        $this->imported = $writer->inserted;

        $this->stats = [
            'Processed:' => $validation->processed,
            'Successfully imported:' => $this->imported,
            'Errors:' => count($this->errors)
        ];


        if ($this->errors) {
            $table = new Table($output);
            $table->setStyle('compact');
            $writer = new TableWriter($table);

            $arrayReader = new ArrayReader($this->errors);

            $errorWorkflow = $this->getWorkflow($arrayReader, $writer);
            $errorWorkflow->process();
        }

        return $this->stats;
    }

    private function getCsvReader(\SplFileObject $file)
    {
        $reader = new CsvReader($file);
        $reader->setHeaderRowNumber(0);
        return $reader;
    }

    private function getWorkflow($reader, $writer = null, $steps = [])
    {
        $workflow = new StepAggregator($reader);
        $workflow->setSkipItemOnFailure(true);
        foreach ($steps as $step) {
            $workflow->addStep($step);
        }
        $workflow->addWriter($writer);
        return $workflow;
    }
}
