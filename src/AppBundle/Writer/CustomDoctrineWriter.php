<?php

namespace AppBundle\Writer;

use Doctrine\Common\Persistence\ObjectManager;
use Port\Doctrine\DoctrineWriter;

class CustomDoctrineWriter extends DoctrineWriter
{
    private $itemCount;
    private $flushSize = 20;
    private $validator;
    private $errors = [];
    public $failedItems = [];
    public $inserted;
    private $isTest;

    public function __construct(ObjectManager $objectManager, $objectName, $validator, $isTest, $index = null, $lookupMethod = 'findOneBy')
    {
        parent::__construct($objectManager, $objectName, $index, $lookupMethod);
        $this->validator = $validator;
        $this->isTest = $isTest;
    }

    public function writeItem(array $item)
    {
        $object = $this->findOrCreateItem($item);

        $this->loadAssociationObjectsToObject($item, $object);
        $this->updateObject($item, $object);
        $this->errors = $this->validator->validate($object);

        if (count($this->errors) > 0) {
            $this->failedItems[] = $this->reformatItem($item);
            return $this;
        }
        $this->inserted++;
        if (!$this->isTest) {
            $this->objectManager->persist($object);

            $this->itemCount++;

            if ($this->itemCount === $this->flushSize) {
                $this->flush();
            }
        }

        return $this;
    }

    public function flush()
    {
        $this->itemCount = 0;

        parent::flush();
    }

    private function reformatItem($item)
    {
        if (isset($item['added'])) {
            unset($item['added']);
        }
        if (isset($item['timestamp'])) {
            unset($item['timestamp']);
        }
        if (isset($item['discontinued'])) {
            $item['discontinued'] = $item['discontinued']->format('Y-m-d');
        }
        return $item;
    }
}