<?php

namespace AppBundle\Steps;

use Port\Exception\UnexpectedValueException;
use Port\Steps\Step;

class ValidationStep implements Step
{
    public $failedProducts = [];
    public $validProducts = [];
    public $validProductsCodes = [];
    public $processed;

    public function process($item, callable $next)
    {
        $this->processed++;
        if (((!$this->priceValidation($item)) || ($this->isInArray($item)))) {
            $this->failedProducts[] = $item;
            throw new UnexpectedValueException("Wrong value");
        }

        $this->validProductsCodes[] = $item['productCode'];
        $this->validProducts[] = $item;
        return $next($item);
    }

    private function priceValidation($item)
    {
        return (is_numeric($item['price']) && is_numeric($item['stock']));
    }

    private function isInArray($item)
    {
        return in_array($item['productCode'], $this->validProductsCodes);
    }
}
