<?php

namespace AppBundle\Steps;

use Port\Steps\Step;

class MapConversionStep implements Step
{
    private $mapping = [];

    public function __construct(array $mapping)
    {
        $this->mapping = $mapping;
    }

    public function process($item, callable $next)
    {
        foreach ($this->mapping as $newKey=>$oldKey) {
            $item = $this->changeKeys($item, $oldKey, $newKey);
        }

        return $next($item);
    }

    private function changeKeys($array, $oldKey, $newKey)
    {
        $array[$newKey] = $array[$oldKey];
        unset($array[$oldKey]);
        return $array;
    }
}
