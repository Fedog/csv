<?php

namespace AppBundle\Steps;

use Port\Steps\Step;

class DateConversionStep implements Step
{
    protected $date;

    public function __construct()
    {
        $this->date = new \DateTime();
    }

    public function process($item, callable $next)
    {
        if ($item['discontinued'] === 'yes') {
            $item['discontinued'] = $this->date;
        } else {
            unset($item['discontinued']);
        }
        $item['timestamp'] = $this->date;
        $item['added'] = $this->date;

        return $next($item);
    }
}
