<?php

namespace AppBundle\Tests;

use AppBundle\Importer\CsvImporter;
use PHPUnit_Framework_TestCase;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Output\StreamOutput;

class ImportServiceTest extends PHPUnit_Framework_TestCase
{
    private $kernel;
    private $container;
    private $em;
    private $mapping;
    private $validator;

    public function setUp()
    {
        $this->kernel = new \AppKernel('dev', true);
        $this->kernel->boot();
        $this->container = $this->kernel->getContainer();
        $this->em = $this->container->get('doctrine')->getManager();
        $this->mapping = $this->container->getParameter('mapping');
        $this->validator = $this->container->get('validator');
    }

    public function testImportedItems()
    {
        $output = new ConsoleOutput();
        $importer = new CsvImporter($this->em, $this->mapping, $this->validator);
        $importer->importCSV('app/Resources/stock.csv', $output, true);
        $this->assertEquals(23, $importer->imported);
    }

    public function testFailedItems()
    {

        $output = new StreamOutput();
        $importer = new CsvImporter($this->em, $this->mapping, $this->validator);
        $importer->importCSV('app/Resources/stock.csv', $output, true);
        $this->assertEquals(6, count($importer->errors));
    }
}
