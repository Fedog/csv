<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class CsvCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:csv')
            ->setDescription('Import csv file.')
            ->setHelp('This command allows you to import a csv file in database')
            ->addArgument('filePath', InputArgument::REQUIRED, 'Path to csv file')
            ->addOption('test', null, InputOption::VALUE_NONE, 'Process file without database inserts')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!is_readable($input->getArgument('filePath'))) {
            $logger = $this->getContainer()->get('logger');
            $logger->error('File not found');
            exit;
        }
        $this
            ->getContainer()
            ->get('app.csv_import')
            ->importCsv($input->getArgument('filePath'), $output, $input->getOption('test'));
        foreach ($this->getContainer()->get('app.csv_import')->stats as $parameter=>$amount) {
            $output->writeln($parameter . $amount);
        }
    }
}
